# OpenML dataset: usp05-ft

https://www.openml.org/d/1057

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This is a PROMISE Software Engineering Repository data set made publicly
available in order to encourage repeatable, verifiable, refutable, and/or
improvable predictive models of software engineering.

If you publish material based on PROMISE data sets then, please
follow the acknowledgment guidelines posted on the PROMISE repository
web page http://promisedata.org/repository .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
(c) 2007  Jingzhou Li
(jingli@ucalgary.ca)
This data set is distributed under the
Creative Commons Attribution-Share Alike 3.0 License
http://creativecommons.org/licenses/by-sa/3.0/

You are free:

* to Share -- copy, distribute and transmit the work
* to Remix -- to adapt the work

Under the following conditions:

Attribution. You must attribute the work in the manner specified by
the author or licensor (but not in any way that suggests that they endorse
you or your use of the work).

Share Alike. If you alter, transform, or build upon this work, you
may distribute the resulting work only under the same, similar or a
compatible license.

* For any reuse or distribution, you must make clear to others the
license terms of this work.
* Any of the above conditions can be waived if you get permission from
the copyright holder.
* Apart from the remix rights granted under this license, nothing in
this license impairs or restricts the author's moral rights.

1. Title: USP05-FT: Software effort estimation at feature level

2. Source Information
-- Donor: Jingzhou Li (jingli@ucalgary.ca), Guenther Ruhe (ruhe@ucalgary.ca)
computer science department
University of Calgary, Canada
(403) 210-5440
-- Date: December 2005

3. Past Usage:
[1]. J.Z. Li, G. Ruhe, A. Al-Emran, M. M. Ritcher, "A Flexible Method for Effort Estimation by Analogy", Empirical Software Engineering, Vol. 12, No. 1, 2007, pp 65-106.
[2]. J.Z. Li, G. Ruhe, "A Comparative Study of Attribute Weighting Heuristics for Effort Estimation by Analogy", Proceedings of the ACM-IEEE International Symposium on Empirical Software Engineering (ISESE'06), September 2006, Brazil.

4. Relevant Information:
-- This data set was part of USP05 that was collected from university student projects about Web and client/server applications
-- The detailed description of the whole data set can be found in reference [1].

5. Number of Instances: 76 (features)

6. Number of Attributes: 15 (including ID, Effort is the actual effort)

7. Attribute Information:
1. ID: Three digit Object ID,
2. Effort: Actual effort in hours expended on tasks related to implementing the object by all participating persons.
3. IntComplx: Complexity of Internal Calculation (1-VeryLow, 2-Low, 3-Medium, 4-High, 5-VeryHigh )
4. DataFile: Number of Data Files/Database Tables Accessed (Positive integer)
5. DataEn: Number of Data Entry Items (Positive integer)
6. DataOut: Number of Data Output Items (Positive integer)
7. UFP: Unadjusted Function Point Count (Positive integer)
8. Lang: Language Used (C++, Java, VB, Java Script, VB Script,  SQL, Php, Perl, Asp, Html, XML, Others)
9. Tools: Development Tools and Platforms (VJ++, VB, Delphi, VisualCafe, JUnit,   PowerBuilder, BorlandC++, Others)
10. ToolExpr: Language and Tool Experience Level (Range of number of months of experience, e.g. [2, 5] for 2 to 5 months, as the minimum experience level is 2 and 5 the maximum in the team)
11. AppExpr: Applications Experience Level (1-VeryLow, 2-Low, 3-Medium, 4-High, 5-VeryHigh)
12. TeamSize: Team size for implementing the object (Range: [a, b], min-max number of persons, e.g. [2, 5])
13. DBMS: Database Systems (Oracle, Access, SQLServer, MySQL, Others)
14. Method: Methodology (OO, SA, SD, RAD, JAD, MVC, Others)
15. AppType: Type of System/Application Architecture (B/S, C/S, BC/S, Centered, Other)


8. Missing Attribute Values: 37

9. Data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1057) of an [OpenML dataset](https://www.openml.org/d/1057). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1057/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1057/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1057/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

